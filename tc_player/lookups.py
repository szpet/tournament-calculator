from ajax_select import register, LookupChannel

from django.db.models import Q
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from tc_player.models import Player


@register('PlayerAutocomplete')
class PlayerLookup(LookupChannel):

    model = Player

    def get_query(self, q, request):
        try:
            users = User.objects.filter(Q(username__startswith=q) | Q(last_name__startswith=q) | Q(first_name__startswith=q))
        except ObjectDoesNotExist:
            return []
        qs = Player.objects.filter(user__in=users)
        return qs

    def format_item_display(self, item):
        return u"<span class='tag'>{0}</span>".format(item)

    def check_auth(self, request):
        if not request.user.is_authenticated():
            raise PermissionDenied
