from django.contrib import admin

# Register your models here.
class PlayerAdmin(admin.ModelAdmin):
    list_display = ("user", "city", "getLastLogin")

    def getLastLogin(self, obj):
        return obj.user.last_login

    getLastLogin.short_description = 'Last Login'
    getLastLogin.admin_order_field = 'user__last_login'


class ITSPinAdmin(admin.ModelAdmin):
    list_display = ['player', 'its_pin']

from . import models
admin.site.register(models.Player, PlayerAdmin)
admin.site.register(models.ITSPin, ITSPinAdmin)