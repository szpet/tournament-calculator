from django import template
from django.core.exceptions import ObjectDoesNotExist

from tc_player.models import ITSPin

register = template.Library()


@register.simple_tag
def loggedUser(user):
    if user.is_anonymous():
        return ""
    return "Logged in as: " + str(user)


@register.simple_tag
def getPlayerITS(player):
    try:
        its_pin = ITSPin.objects.filter(player=player).get()
        return its_pin.its_pin
    except ObjectDoesNotExist:
        return None
