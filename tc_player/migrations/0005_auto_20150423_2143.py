# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_player', '0004_player_city'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='city',
            field=models.ForeignKey(default=b'1', verbose_name=b'City', blank=True, to='tc_base.City'),
        ),
    ]
