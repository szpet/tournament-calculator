# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_base', '0004_army'),
        ('tc_player', '0003_remove_player_city'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='city',
            field=models.ForeignKey(verbose_name=b'City', blank=True, to='tc_base.City', null=True),
            preserve_default=True,
        ),
    ]
