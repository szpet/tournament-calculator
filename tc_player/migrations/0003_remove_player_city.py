# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_player', '0002_auto_20150124_1544'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='player',
            name='city',
        ),
    ]
