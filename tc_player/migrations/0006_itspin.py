# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

from tc_player.models import Player
from tc_player.models import ITSPin

def copy_data(apps, schema_editor):
    data = [
        ["1","R2992"],
        ["3","T5783"],
        ["4","F6917"],
        ["5 ","P6244"],
        ["6 ","F8592"],
        ["7 ","L2944"],
        ["8 ","D5640"],
        ["11","P4966"],
        ["12","N5297"],
        ["13","R0879"],
        ["18","U1513"],
        ["23","D3002"],
        ["24","Z2771"],
        ["26","F5586"],
        ["27","X4127"],
        ["29","A0648"],
        ["30","X0937"],
        ["32","Y3177"],
        ["33","O4475"],
        ["35","U2495"],
        ["44","K7482"],
        ["45","I6188"],
        ["48","P4751"],
        ["50","A7253"],
        ["52","L551x"],
        ["53","D7002"],
        ["54","R1886"],
        ["55","Q3065"],
        ["56","E8676"],
        ["57","W8389"],
        ["61","S1285"],
        ["62","C5841"],
        ["63","N5603"],
        ["64","W8926"]
    ]
    for player, itspin in data:
        itsPinObj = ITSPin(player_id=player, its_pin=itspin)
        itsPinObj.save()

class Migration(migrations.Migration):

    dependencies = [
        ('tc_player', '0005_auto_20150423_2143'),
    ]

    operations = [
        migrations.CreateModel(
            name='ITSPin',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('its_pin', models.CharField(max_length=10, null=True)),
                ('player', models.ForeignKey(verbose_name=b'Player', to='tc_player.Player', unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RunPython(copy_data),
    ]
