from django.test import TestCase
from django.contrib.auth.models import User
from tc_player.models import Player


# Create your tests here.

class TestPlayer(TestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(username='jacob1', email='jacob1@test.pl', password='top_secret')
        self.user2 = User.objects.create_user(username='jacob2', email='jacob2@test.pl', password='top_secret')
        self.player_1 = Player.objects.create(user=self.user1)
        self.player_2 = Player.objects.create(user=self.user2)