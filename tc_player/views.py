from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.forms import Form
from django.forms import HiddenInput
from django.forms import CharField
from django.forms import PasswordInput
from django.views.generic.edit import UpdateView
from django.views.generic.edit import CreateView
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.db.models import Q
from django.core.mail import EmailMessage
import string
import random

from tc_player.models import Player
from tc_player.models import ITSPin
from forms import UserForm
from forms import RemindPasswordForm

# Create your views here.


class ChangePasswordForm(Form):
    new_password = CharField(widget=PasswordInput(), label="New Password")
    new_password_confirm = CharField(widget=PasswordInput(), label="Confirm")

    def __init__(self, user, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.user = user

    def clean(self):
        if not self.cleaned_data.get('new_password') == self.cleaned_data.get('new_password_confirm'):
            self.add_error('new_password_confirm', "Passwords do not match")

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data["new_password"])
        if commit:
            self.user.save()
        return self.user


def changePassword(request):
    if request.method == 'POST':
        changePasswordForm = ChangePasswordForm(data=request.POST, user=request.user)
        if changePasswordForm.is_valid():
            changePasswordForm.save()
            return HttpResponseRedirect("/logout/")
    else:
        changePasswordForm = ChangePasswordForm(user=request.user)
    return render(request, "changePassword.html", dict(form=changePasswordForm))


def addUser(request):
    if request.method == 'POST':
        uf = UserForm(request.POST, prefix='user')
        if uf.is_valid():
            user = uf.save()
            pf = Player(user=user)
            pf.save()
            return HttpResponseRedirect("/tc_player/addUserConfirm/")
    else:
        uf = UserForm(prefix='user')
    return render(request, 'addUser.html', dict(form=uf))

def addUserConfirm(request):
    return render(request, "confirm.html", {})

class UpdatePlayer(UpdateView):
    model = Player
    fields = ['city']

class UpdateITSPin(UpdateView):
    model = ITSPin
    fields = ['its_pin']
    template_name_suffix = '_update_form'

    def get_form(self):
        form = super(UpdateITSPin, self).get_form()
        return form

    def get_success_url(self):
        return reverse("UpdateITSPin", args=(self.object.id,))

class CreateITSPin(CreateView):
    model = ITSPin
    fields = ['its_pin']

    def get_form(self):
        form = super(CreateITSPin, self).get_form()
        return form

    def get_success_url(self):
        return reverse("UpdateITSPin", args=(self.object.id,))



def random_password(length=10):
    chars = string.ascii_uppercase + string.digits + string.ascii_lowercase
    password = ''.join(random.choice(chars) for _ in range(length))
    return password


def RemindPassword(request):
    if request.method == 'POST':
        remindPasswordForm = RemindPasswordForm(request.POST)
        if remindPasswordForm.is_valid():
            username_mail = request.POST['username_mail']
            user = User.objects.filter(Q(username=username_mail) | Q(email=username_mail))
            if user:
                for p in user:
                    password = random_password()
                    email_text = """Hi,
                     
New password for {0} "{1}" {2} is {3} 
                                    
Tournament Calculator admin (Bartosz Skorupa)         
                                 """.format(p.first_name, p.username, p.last_name, password)
                    email = EmailMessage('Tournament Calculator New password', email_text, to=[p.email],
                                         from_email="turniej@infinity.wroclaw.pl",
                                         reply_to=["bartosz@skorupa.net"])
                    email.send()
                    p.set_password(password)
                    p.save()
                return HttpResponseRedirect(reverse("RemindPassword"))
            else:
                remindPasswordForm.add_error('username_mail', u'User not found')
    else:
        remindPasswordForm = RemindPasswordForm()
    return render(request, 'RemindPassword.html', dict(form=remindPasswordForm))