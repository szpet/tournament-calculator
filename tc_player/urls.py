from django.conf.urls import url

from . import views


urlpatterns = [
        url(r'^addUser$', views.addUser),
        url(r'changePassword/', views.changePassword, name="changePassword"),
        url(r'addUserConfirm/', views.addUserConfirm, name="addUserConfirm"),
        url(r'editPlayer/(?P<pk>\d+)/',  views.UpdatePlayer.as_view(success_url="/"), name='updatePlayer'),
        url(r'UpdateITSPin/(?P<pk>\d+)/',  views.UpdateITSPin.as_view(success_url="/"), name='UpdateITSPin'),
        url(r'CreateITSPin/',  views.CreateITSPin.as_view(success_url="/"), name='CreateITSPin'),
        url(r'remindPassword', views.RemindPassword, name="RemindPassword")
    ]
