# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

from tc_base.models import City

# Create your models here.
class Player(models.Model):
    user = models.OneToOneField(User, verbose_name='Player')
    city = models.ForeignKey(City, verbose_name="City", blank=True, default="1")

    def __str__(self):
        return u"{0} \"{1}\" {2}".format(self.user.first_name, self.user.username, self.user.last_name)

    def __unicode__(self):
        return u"{0} \"{1}\" {2}".format(self.user.first_name, self.user.username, self.user.last_name)

class ITSPin(models.Model):
    its_pin = models.CharField(max_length=10, null=True)
    player = models.OneToOneField(Player, verbose_name="Player")

    def __str__(self):
        return u'{} - {}'.format(self.player, self.its_pin)

    def __unicode__(self):
        return u'{} - {}'.format(self.player, self.its_pin)
