from django import forms
from news.models import *

class ContactForm(forms.Form):
  subject = forms.CharField(max_length=100)
  message = forms.CharField()
  sender = forms.EmailField()
  cc_myself = forms.BooleanField(required=False)