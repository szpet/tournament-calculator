#from django.conf.urls.defaults import *
from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^post_comment/(?P<id>[0-9]+)/$', views.show_news),
    url(r'^(?P<id>[0-9]+)/$', views.show_news),
    url(r'^(?P<slug>[\w\-_]+)/$', views.news_by_category),
    ]
