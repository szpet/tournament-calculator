# -*- coding: utf-8 -*-
from django.utils import timezone
from django.template import RequestContext
from django.shortcuts import render
from news.models import *
from news.forms import *
from django import forms

def index(request):
  news = News.objects.filter(date__lt=timezone.now()).order_by('-id')
  return render('news_by_category.html', {'news': news, 'paginate_by': 10})

def news_by_category(request, slug):
  c = Category.objects.get(slug=slug)
  news = c.news_set.filter(date__lt=timezone.now()).order_by('-id')
  return render(request, 'news_by_category.html', {'news': news, 'c': c, 'paginate_by': 10})



class CommentForm(forms.ModelForm):
  class Meta:
    model = NewsComments
    exclude = ('news',)

def show_news(request, id):
  news = News.objects.get(id=id)
  if request.method == 'POST':
    comment=NewsComments(news=news)
    form = CommentForm(request.POST, instance=comment)
    form.save()
  else:
    form = CommentForm()
  comment = NewsComments.objects.filter(news=news.id)
  files = NewsFiles.objects.filter(news=news.id)
  return render(request, 'show_news.html', {'news':news, 'comment': comment, 'files': files, 'form':form.as_p()})

def rss_cat(request, slug):
  c = Category.objects.get(slug=slug)
  news = c.news_set.filter(date__lt=timezone.now()).order_by('-id')
  return render(request, 'rss_cat.html', {'category':c, 'news':news})

def print_news(request, id):
  news = News.objects.get(id=id)
  return render(request, 'news_print.html', {'news':news})
