# -*- coding: utf-8 -*-
from django.db import models
from datetime import datetime
from django.contrib.auth.models import User

# Create your models here.
class Category(models.Model):
  name = models.CharField(max_length=255, verbose_name='Nazwa Kategorii')
  slug = models.SlugField(max_length=255, unique=True, verbose_name='Odnośnik')
  icon =  models.ImageField(upload_to='icons', verbose_name='Ikonka Kategorii', blank=True)
  class Meta:
    verbose_name = "Kategoria"
    verbose_name_plural = "Kategorie"
  def __str__(self):
    return self.name
  def __unicode__(self):
    return self.name

class News(models.Model):
  category = models.ManyToManyField(Category, verbose_name='Kategorie')
  title = models.CharField(max_length=255, verbose_name='Tytuł')
  slug = models.SlugField(max_length=255, unique=True, verbose_name='Odnośnik')
  text = models.TextField(verbose_name='Treść')
  date = models.DateTimeField(default=datetime.now(),verbose_name='Data dodania')
  wykop = models.CharField(max_length=255, verbose_name='Wykop', blank=True)
  author = models.ForeignKey(User, default=1)
  class Meta:
    verbose_name = "Wiadomość"
    verbose_name_plural = "Wiadomości"
  def __str__(self):
    return self.title
  def __unicode__(self):
    return self.title
  def get_absolute_url(self):
    return '/news/' + self.slug + '/'

class NewsComments(models.Model):
  news = models.ForeignKey(News)
  text = models.TextField(verbose_name='Treść')
  author = models.CharField(max_length=255, verbose_name='Autor', blank=True)

class NewsFiles(models.Model):
  news = models.ForeignKey(News)
  fileDesc = models.CharField(max_length=100, verbose_name='Krótki opis')
  file = models.FileField(upload_to='NewsFile', max_length=256)