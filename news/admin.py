# -*- coding: utf-8 -*-
from django.contrib import admin

from news.models import *

# dla kategorii
class CategoryAdmin(admin.ModelAdmin):
  list_display = ('name','icon')
  prepopulated_fields = {'slug': ('name',)}

class CommentInline(admin.StackedInline):
  model = NewsComments
  extra = 0

class FileInline(admin.StackedInline):
  model = NewsFiles
  extra = 0

# dla wiadomości
class NewsAdmin(admin.ModelAdmin):
  list_display = ('title','date','author')
  prepopulated_fields = {'slug': ('title',)}
  inlines = [CommentInline, FileInline]
  list_filter = ('category',)
  def formfield_for_foreignkey(self, db_field, request, **kwargs):
    if db_field.name == 'author':
      kwargs['initial'] = request.user.id
      return db_field.formfield(**kwargs)
  class Media:
    js = ('/static/jsc/tiny_mce/tiny_mce.js', '/static/jsc/tiny_mce/textareas.js')

#dla komentarzy
#class CommentAdmin(admin.ModelAdmin):
#  list_display = ('news','author')

# rejestracja wraz z podaniem klasy konfigurującej PA
admin.site.register(Category, CategoryAdmin)
admin.site.register(News, NewsAdmin)
#admin.site.register(NewsComments, CommentAdmin)