from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.contrib.auth.models import User

from datetime import datetime

from tc_player import models as tcPlayer
from tc_base.models import Games
from tc_base.models import Army
from tc_base.models import City

# Create your models here.

class League(models.Model):
    STATE = ( ("new", "before starting"),
        ("ongoing", "ongoing"),
        ("completed", "completed"))
    name = models.CharField(max_length= 250)
    status = models.CharField(max_length=50, choices=STATE, verbose_name="Status", default="new")
    judges = models.ManyToManyField(User, verbose_name="Judge", blank=True, related_name="league_judge")
    players = models.ManyToManyField(tcPlayer.Player, blank=True)
    game = models.ForeignKey(Games, blank=True, null=True)
    city = models.ForeignKey(City, blank=False, null=False)
    statureUrl = models.URLField(blank=False, null=False, default="", verbose_name="Url to statue")

    def __str__(self):
        return u'{}'.format(self.name)

    def __unicode__(self):
        return u'{}'.format(self.name)


class Pair(models.Model):
    player_1 = models.ForeignKey(tcPlayer.Player, related_name="league_player_1")
    player_2 = models.ForeignKey(tcPlayer.Player, related_name="league_player_2")

    def __unicode__(self):
        return u'{} vs. {}'.format(self.player_1, self.player_2)

    def __str__(self):
        return u'{} vs. {}'.format(self.player_1, self.player_2)

    def canEdit(self, user, id):
        if user.is_anonymous():
            return False
        b1 = Pair.objects.filter(Q(player_1=user) | Q(player_2=user), id=id).count() == 1
        b2 = self.__getResult(user) is not None
        return b1 and b2

    def __getResult(self, player):
        try:
            return Result.objects.get(player=player, pair=self)
        except ObjectDoesNotExist:
            return None

    def getResultPlayer1(self):
        return self.__getResult(self.player_1) if self.__getResult(self.player_1) != None else "--"

    def getResultPlayer2(self):
        return self.__getResult(self.player_2) if self.__getResult(self.player_2) != None else "--"

class Round(models.Model):
    league = models.ForeignKey(League, related_name="round_to_league")
    pairs = models.ManyToManyField(Pair, blank=True)
    start_date = models.DateField(default=datetime.now, blank=True)
    end_date = models.DateField(null=True)
    text = models.TextField(verbose_name='Description', null=True)

    def __str__(self):
        return str(self.start_date) + " - " + str(self.end_date)

    def __unicode__(self):
        return str(self.start_date) + " - " + str(self.end_date)

class Achievements(models.Model):
    name = models.CharField(max_length= 250)
    league = models.ForeignKey(League, related_name="Achievement")
    points = models.IntegerField(default=0)
    text = models.TextField(verbose_name='Description', null=True)

    def __str__(self):
        return u'{}'.format(self.name)
    def __unicode__(self):
        return u'{}'.format(self.name)

class Result(models.Model):
    big_p = models.IntegerField(default=0)
    small_p = models.IntegerField(default=0)
    tournament_p = models.IntegerField(default=0)
    pair = models.ForeignKey(Pair)
    player = models.ForeignKey(tcPlayer.Player, related_name="league_player")
    army = models.ForeignKey(Army, default=1)
    achievements = models.ManyToManyField(Achievements)

    def __str__(self):
        return str(self.big_p) + "(" + str(self.small_p) + ")"

    def __unicode__(self):
        return unicode(self.big_p) + "(" + unicode(self.small_p) + ")"