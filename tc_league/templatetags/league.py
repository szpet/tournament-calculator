from django import template
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django.template.loader import get_template
from datetime import date

register = template.Library()

from tc_player import models as tcPlayer
from tc_league.models import Result
from tc_league.models import Round
from tc_league.models import Achievements
from tc_base.models import Army


def createResult(result):
    achievementsList = ""
    for achievement in result.achievements.all():
        achievementsList = unicode(achievementsList) + u'<li class="small">' + unicode(achievement.name) + u"</li>"
    return unicode(result.tournament_p) + u"/" + unicode(result.big_p) + u"/" +\
           unicode(result.small_p) + u"<br><small>" +\
           unicode(result.army) + u'</small><ul class="players_classification">' + unicode(achievementsList) + u"</ul>"


@register.simple_tag(takes_context=True)
def getResult(context, pair, player, user, league, round):
    try:
        result = Result.objects.filter(pair=pair, player=player).get()
        if canShowResultForm(round, player, user, league):
            return createResultForm(context, league, pair, player, result.big_p, result.small_p, result.tournament_p, result)
        return createResult(result)
    except ObjectDoesNotExist:
        if canShowResultForm(round, player, user, league):
            return createResultForm(context, league, pair, player, 0, 0, 0, None)
        return "0/0/0"
    return "0/0/0"


def canShowResultForm(round, player, user, league):
    return isRoundOpen(round) and (isResultOwner(player, user) or isJudge(league, user) )


def isRoundOpen(round):
    return round.start_date <= date.today() and date.today() <= round.end_date


def isResultOwner(player, user):
    #return str(player.user.id) +" == "+ str(user.id) + " = " + str( player.user.id == user.id )
    return player.user.id == user.id


def isJudge(league, user):
    return user in league.judges.all()


def createResultForm(context, league, pair, player, big_p, small_p, victory_p, result):
    armies = Army.objects.filter(game=league.game, visible=True).all()
    achievements = Achievements.objects.filter(league=league).all()
    if result is not None:
        for achievement in achievements:
            if achievement in result.achievements.all():
                achievement.selected = True
            else:
                achievement.selected = False

    return render_to_string('tc_league/addResults.html',
                    request = context['request'],
                    context = {'pair': pair,
                    'player': player,
                    'league': league,
                    'big_p': big_p,
                    'small_p': small_p,
                    'victory_p': victory_p,
                    'armies': armies,
                    'selectedArmyId': result.army.id if result is not None and result.army is not None else -1,
                    'achievements': achievements
                    }
    )


@register.simple_tag
def getSumResult(player, league):
    raunds = Round.objects.filter(league=league)
    big = 0
    small = 0
    victory = 0
    for raund in raunds:
        for pair in raund.pairs.all():
            try:
                result = Result.objects.filter(pair=pair, player=player).get()
                victory = victory + result.tournament_p
                big = big + result.big_p
                small = small + result.small_p
                for achievement in result.achievements.all():
                    victory = victory + achievement.points
            except:
                pass
    return str(victory) + "/" + str(big)+"/"+str(small)

@register.simple_tag
def getPointsResult(player, league):
    raunds = Round.objects.filter(league=league)
    big = 0
    small = 0
    victory = 0
    for raund in raunds:
        for pair in raund.pairs.all():
            try:
                result = Result.objects.filter(pair=pair, player=player).get()
                victory = victory + result.tournament_p
                big = big + result.big_p
                small = small + result.small_p
            except:
                pass
    return str(victory) + "/" + str(big) + "/" + str(small)

@register.simple_tag
def getPointsAchievements(player, league):
    raunds = Round.objects.filter(league=league)
    big = 0
    for raund in raunds:
        for pair in raund.pairs.all():
            try:
                result = Result.objects.filter(pair=pair, player=player).get()
                for achievement in result.achievements.all():
                    big = big + achievement.points
            except:
                pass
    return str(big)

@register.assignment_tag
def isJudge(league, user):
    return user in league.judges.all()

@register.assignment_tag
def canSingIn(league, user):
    if user.is_anonymous():
        return False
    player = tcPlayer.Player.objects.filter(user=user).get()
    return player not in league.players.all()
