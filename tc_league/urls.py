#from django.conf.urls.defaults import *
from django.conf.urls import url

from . import views
urlpatterns = [
        # url(r'addResult/(\d+)/', views.addResult, name="addResult"),
        # url(r'editResult/(\d+)/', views.editResult, name="editResult"),
        url(r'addLeague', views.LeagueView.as_view(success_url="tc_league/leagueDetails/(?P<id>\w+)/"), name="league-new"),
        url(r'deleteLeague/(?P<pk>\d+)/', views.deleteLeague, name="deleteLeague"),
        url(r'leagueList/$', views.LeagueList.as_view(), name="leagueList"),
        url(r'leagueDetails/(?P<pk>\d+)/$', views.LeagueDetails.as_view(), name="leagueDetails"),
        url(r'leagueEdt/(?P<pk>\d+)/$', views.LeagueEdit.as_view(), name="league-edite"),
        url(r'leagueRoundAdd/', views.LeagueRoundAdd.as_view(), name="LeagueRound-new"),
        url(r'leaguePairAdd/', views.LeaguePairAdd.as_view(), name="LeagueRoundPair-new"),
        url(r'leageAddResult/', views.addResult, name="LeagueResultAdd"),
        url(r'registerPlayer/(\d+)/', views.registerPlayer, name="registerPlayer"),
        url(r'daletePair/(\d+)/(\d+)/', views.deletePair, name="deletePair"),
        url(r'achievementAdd/', views.AchievementsAdd.as_view(), name="Achievements-new"),
        url(r'achievementUpdate/(?P<pk>\d+)/', views.AchievementsUpdate.as_view(), name="Achievements-update"),
        url(r'editRound/(?P<pk>\d+)/', views.LeagueRoundEdit.as_view(), name="LeagueRound-edit"),
        url(r'buildLeagueRoundsForm/(?P<pk>\d+)/', views.buildLeagueRoundsForm, name="buildLeagueRoundsForm"),
        url(r'buildLeagueRounds/(?P<pk>\d+)/', views.buildLeagueRounds, name="buildLeagueRounds")
    ]
