
from django.forms import ModelForm

from ajax_select.fields import AutoCompleteSelectField, AutoCompleteSelectMultipleField
from models import League


class NewLeagueForm(ModelForm):
    class Meta:
        model = League
        fields = ['name', 'judges', 'players', 'game', 'city', 'statureUrl']

    judges = AutoCompleteSelectMultipleField('PlayerAutocomplete', required=True, help_text=None)
    players = AutoCompleteSelectMultipleField('PlayerAutocomplete', required=True, help_text=None)