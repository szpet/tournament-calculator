from datetime import datetime
from datetime import timedelta

from django.contrib.admin.widgets import AdminDateWidget
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.forms import CharField
from django.forms import DateField
from django.forms import Form
from django.forms import HiddenInput
from django.forms import ModelChoiceField
from django.forms import ModelForm
from django.forms import Select
from django.forms import ValidationError
from django.http import Http404
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView

from ajax_select.fields import AutoCompleteSelectField, AutoCompleteSelectMultipleField

from tc_league.models import Pair
from tc_league.models import League
from tc_league.models import Result
from tc_league.models import Round
from tc_league.models import Achievements
from tc_player.models import Player
from tc_base.models import Army
from forms import NewLeagueForm

# Create your views here.

class AddResultForm(ModelForm):
    big_p = CharField(label="Points", max_length=5, required=True)
    small_p = CharField(label="Small points", max_length=5, required=True)
    tournament_p = CharField(label="Victory points", max_length=5, required=True)
    army = ModelChoiceField(label="Army", queryset=[], widget=Select())

    def __init__(self, game, *args, **kwargs):
        super(AddResultForm, self).__init__(*args, **kwargs)
        self.fields['army'].queryset = Army.objects.filter(game=game)

    class Meta:
        model = Result
        fields = ['tournament_p', 'big_p', 'small_p', 'army']

def getResult(pair, player):
    try:
        return Result.objects.get(player=player, pair=pair)
    except ObjectDoesNotExist:
        return None

def viewGames(request):
    pairs = Pair.objects.all().order_by("-id")[:20]
    for p in pairs:
        p.Result1 = getResult(p, p.player_1)
        p.Result2 = getResult(p, p.player_2)

    return render_to_response("viewGames.html", {'games':pairs}, context_instance=RequestContext(request))

class LeagueView(CreateView):
    model = League
    form_class = NewLeagueForm

    def form_valid(self, form):
        league = form.save()
        league.save()
        self.id = league.id
        league.judges.add(self.request.user)
        league.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("leagueDetails", args=(self.id,))

class LeagueList(ListView):
    model = League

class LeagueDetails(DetailView):
    model = League

    def get_object(self):
        # Call the superclass
        object = super(LeagueDetails, self).get_object()
        object.rounds = self.getRounds(object)
        object.achievements = Achievements.objects.filter(league=object)
        return object

    def getRounds(self, league):
        user = self.request.user
        if user in league.judges.all():
            rounds = Round.objects.filter(league=league)
            return rounds
        rounds = Round.objects.filter(league=league, start_date__lte=datetime.now())
        return rounds


class LeagueEdit(UpdateView):
    model = League
    fields = ['name', 'status', 'judges', 'players', 'game', 'city', 'statureUrl']
    template_name_suffix = '_update_form'

    def get_form(self, form_class=None):
        form = super(LeagueEdit, self).get_form()
        #form.fields['players'].widget = autocomplete.MultipleChoiceWidget('PlayerAutocomplete')
        form.fields['players'].queryset = Player.objects.filter(~Q(id="2"))
        return form

    def get_success_url(self):
        return reverse("leagueDetails", args=(self.object.id,))

class LeagueRoundAdd(CreateView):
    model = Round
    fields = ['start_date', 'end_date', 'text']

    def get_form(self, form_class=None):
        form = super(LeagueRoundAdd, self).get_form(form_class)
        form.fields['start_date'].widget = AdminDateWidget()
        form.fields['end_date'].widget = AdminDateWidget()
        form.lid = self.request.GET['lid']
        return form

    def form_valid(self, form):
        form.instance.league_id = self.request.GET['lid']
        return super(LeagueRoundAdd, self).form_valid(form)

    def get_success_url(self):
        return reverse("leagueDetails", args=(self.request.GET['lid'],))

class LeagueRoundEdit(UpdateView):
    model = Round
    fields = ['start_date', 'end_date', 'text']
    template_name_suffix = '_update_form'

    def get_form(self, form_class=None):
        form = super(LeagueRoundEdit, self).get_form()
        form.fields['start_date'].widget = AdminDateWidget()
        form.fields['end_date'].widget = AdminDateWidget()
        form.lid = self.request.GET['lid']
        return form

    def form_valid(self, form):
        form.instance.league_id = self.request.GET['lid']
        return super(LeagueRoundEdit, self).form_valid(form)

    def get_success_url(self):
        return reverse("leagueDetails", args=(self.request.GET['lid'],))

class LeaguePairAddForm(ModelForm):
    class Meta:
        model = Pair

        fields = ['player_1', 'player_2']

    def clean(self):
        if self.cleaned_data.get('player_1') == self.cleaned_data.get('player_2'):
            raise ValidationError("The pair cannot contain one player")
        return self.cleaned_data

class LeaguePairAdd(CreateView):
    model = Pair
    form_class = LeaguePairAddForm

    def get_form(self, form_class=None):
        form = super(LeaguePairAdd, self).get_form()
        form.fields['player_1'].queryset = Player.objects.filter(~Q(id="2"))
        form.fields['player_2'].queryset = Player.objects.filter(~Q(id="2"))
        self.request.session['LeagueAutocomplete_league_id'] = self.request.GET['lid']
        form.lid = self.request.GET['lid']
        form.rid = self.request.GET['rid']
        return form

    def form_valid(self, form):
        form.date = datetime.now()
        form.game = League.objects.get(id=self.request.GET['lid']).game
        pair = form.save()
        r = Round.objects.get(id=self.request.GET['rid'])
        r.pairs.add(pair)
        return super(LeaguePairAdd, self).form_valid(form)

    def get_success_url(self):
        return reverse("leagueDetails", args=(self.request.GET['lid'],))

def addResult(request):
    if request.method == 'POST':
        try:
            result = Result.objects.get(pair_id = request.POST.get('pair_id'), player_id = request.POST.get('player_id'))
            result.big_p = request.POST.get('big_p')
            result.small_p = request.POST.get('small_p')
            result.tournament_p = request.POST.get('tournament_p')
            result.army_id = request.POST.get('selectedArmy')
            result.save()
            result.achievements.clear()

            for achievement in request.POST.getlist('achievements', default=[]):
                result.achievements.add(Achievements.objects.get(id=achievement))
                result.save()
        except ObjectDoesNotExist:
            result = Result(big_p = request.POST.get('big_p'),
                            small_p = request.POST.get('small_p'),
                            pair_id = request.POST.get('pair_id'),
                            player_id = request.POST.get('player_id'),
                            tournament_p = request.POST.get('tournament_p'),
                            army_id = request.POST.get('selectedArmy'))
            result.save()
            for achievement in request.POST.getlist('achievements', default=[]):
                result.achievements.add(Achievements.objects.get(id=achievement))
            result.save()
        return HttpResponseRedirect("/tc_league/leagueDetails/"+str(request.POST.get('league_id')))
    return HttpResponseRedirect("/")

def registerPlayer(request, leaugeId):
    l = League.objects.get(id=leaugeId)
    l.players.add(Player.objects.get(user=request.user))
    l.save()
    return HttpResponseRedirect("/tc_league/leagueDetails/"+str(leaugeId))

def deletePair(request, pairId, leaugeId):
    Pair.objects.get(id=pairId).delete()
    return HttpResponseRedirect("/tc_league/leagueDetails/"+str(leaugeId))

def deleteLeague(request, pk):
    try:
        League.objects.get(pk=pk).delete()
    except League.DoesNotExist:
        raise Http404("League does not exist")
    return HttpResponseRedirect("/tc_league/leagueList/")

class AchievementsAdd(CreateView):
    model = Achievements
    fields = ['name', 'points', 'text', 'league']

    def get_initial(self):
        return {'league': self.request.GET['lid']}

    def get_form(self, form_class=None):
        form = super(AchievementsAdd, self).get_form()
        form.fields['league'].widget = HiddenInput()
        form.lid =  self.request.GET['lid']
        return form

    def get_success_url(self):
        return reverse("leagueDetails", args=(self.request.GET['lid'],))

class AchievementsUpdate(UpdateView):
    model = Achievements
    fields = ['name', 'points', 'text', 'league']
    template_name_suffix = '_update_form'

    def get_initial(self):
        return {'league': self.request.GET['lid']}

    def get_form(self, form_class=None):
        form = super(AchievementsUpdate, self).get_form()
        form.fields['league'].widget = HiddenInput()
        form.lid = self.request.GET['lid']
        return form

    def get_success_url(self):
        return reverse("leagueDetails", args=(self.request.GET['lid'],))

class BuildLeagueRoundsFormClass(Form):
    start_date = DateField(widget=AdminDateWidget,  label="First round start date", required=True)
    round_duration = CharField(label="Round duration in days", max_length=5, required=True)

def buildLeagueRoundsForm(request, pk):
    form = BuildLeagueRoundsFormClass()
    return render(request, 'tc_league/buildLeagueRoundsForm.html', {'lpk': pk, 'form': form})

def buildLeagueRounds(request, pk):
    if request.method == 'POST':
        if len(request.POST.get('start_date')) == 0 or len(request.POST.get('round_duration')) == 0:
            return render(request, 'tc_league/buildLeagueRoundsForm.html', {'lpk': pk, 'form': BuildLeagueRoundsFormClass()})
        league = League.objects.get(pk=pk)
        N = league.players.count()
        numberOfRounds=N - 1
        rounds = []
        startDate = datetime.strptime(request.POST.get('start_date'), '%Y-%m-%d')
        duration = int(request.POST.get('round_duration'))
        endDate = startDate + timedelta(days=duration)

        playersList=list(league.players.all())
        for roundNumber in range(1,numberOfRounds+1):
            r = Round(league=league, text=" ", start_date = startDate, end_date = endDate)
            startDate = startDate + timedelta(days=duration + 1)
            endDate = startDate + timedelta(days=duration)
            r.save()
            tmp_playersList = playersList[:]
            while len(tmp_playersList) > 0:
                player_1 = tmp_playersList.pop(0)
                player_2 = tmp_playersList.pop()
                r.pairs.create(player_1 = player_1, player_2 = player_2)
            playersList.insert(1, playersList.pop())
            rounds.append(r)

    return HttpResponseRedirect(reverse("leagueDetails", kwargs={'pk': pk}))