# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0011_auto_20150620_1744'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pair',
            name='date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
