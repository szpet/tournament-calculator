# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_base', '0004_army'),
        ('tc_league', '0012_auto_20150620_2126'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pair',
            name='date',
        ),
        migrations.RemoveField(
            model_name='pair',
            name='game',
        ),
        migrations.RemoveField(
            model_name='pair',
            name='player_1_army',
        ),
        migrations.RemoveField(
            model_name='pair',
            name='player_2_army',
        ),
        migrations.AddField(
            model_name='result',
            name='army',
            field=models.ForeignKey(blank=True, to='tc_base.Army', null=True),
        ),
    ]
