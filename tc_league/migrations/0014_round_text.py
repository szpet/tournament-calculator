# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0013_auto_20150621_1748'),
    ]

    operations = [
        migrations.AddField(
            model_name='round',
            name='text',
            field=models.TextField(null=True, verbose_name=b'Description'),
            preserve_default=True,
        ),
    ]
