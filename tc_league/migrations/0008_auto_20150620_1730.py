# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0007_round'),
    ]

    operations = [
        migrations.AddField(
            model_name='round',
            name='end_data',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='round',
            name='start_date',
            field=models.DateField(default=datetime.datetime.now, blank=True),
        ),
    ]
