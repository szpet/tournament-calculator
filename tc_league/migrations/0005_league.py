# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_base', '0004_army'),
        ('tc_player', '0005_auto_20150423_2143'),
        ('tc_league', '0004_auto_20150422_2205'),
    ]

    operations = [
        migrations.CreateModel(
            name='League',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250)),
                ('status', models.CharField(default=b'new', max_length=50, verbose_name=b'Status', choices=[(b'new', b'before starting'), (b'ongoing', b'ongoing'), (b'completed', b'completed')])),
                ('city', models.ForeignKey(to='tc_base.City')),
                ('game', models.ForeignKey(blank=True, to='tc_base.Games', null=True)),
                ('players', models.ManyToManyField(to='tc_player.Player', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
