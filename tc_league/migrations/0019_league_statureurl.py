# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0018_auto_20150628_1101'),
    ]

    operations = [
        migrations.AddField(
            model_name='league',
            name='statureUrl',
            field=models.URLField(default=b'', verbose_name=b'Url to statue'),
            preserve_default=True,
        ),
    ]
