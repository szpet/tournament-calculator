# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0009_auto_20150620_1733'),
    ]

    operations = [
        migrations.AlterField(
            model_name='round',
            name='pairs',
            field=models.ManyToManyField(to='tc_league.Pair', null=True, blank=True),
        ),
    ]
