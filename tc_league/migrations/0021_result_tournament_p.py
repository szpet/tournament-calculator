# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0020_auto_20150628_1114'),
    ]

    operations = [
        migrations.AddField(
            model_name='result',
            name='tournament_p',
            field=models.IntegerField(default=0),
        ),
    ]
