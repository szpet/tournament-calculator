# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0006_league_judges'),
    ]

    operations = [
        migrations.CreateModel(
            name='Round',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('league', models.ForeignKey(related_name='round_to_league', to='tc_league.League')),
                ('pairs', models.ManyToManyField(to='tc_league.Pair')),
            ],
        ),
    ]
