# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0015_achievements'),
    ]

    operations = [
        migrations.AddField(
            model_name='result',
            name='achievements',
            field=models.ManyToManyField(to='tc_league.Achievements', null=True),
            preserve_default=True,
        ),
    ]
