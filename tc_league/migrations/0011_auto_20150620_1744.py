# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0010_auto_20150620_1733'),
    ]

    operations = [
        migrations.RenameField(
            model_name='round',
            old_name='end_data',
            new_name='end_date',
        ),
    ]
