# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0014_round_text'),
    ]

    operations = [
        migrations.CreateModel(
            name='Achievements',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('points', models.IntegerField(default=0)),
                ('text', models.TextField(null=True, verbose_name=b'Description')),
                ('name', models.CharField(max_length=250)),
                ('league', models.ForeignKey(related_name='Achievement', to='tc_league.League')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
