# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0002_auto_20150202_2121'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pair',
            name='game',
            field=models.ForeignKey(related_name='Game', default=b'1', to='tc_base.Games'),
            preserve_default=True,
        ),
    ]
