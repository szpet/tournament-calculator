# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0003_auto_20150209_2032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pair',
            name='player_1_army',
            field=models.ForeignKey(related_name='league_player_1_army', blank=True, to='tc_base.Army', null=True),
        ),
        migrations.AlterField(
            model_name='pair',
            name='player_2_army',
            field=models.ForeignKey(related_name='league_player_2_army', blank=True, to='tc_base.Army', null=True),
        ),
    ]
