# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_player', '0002_auto_20150124_1544'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pair',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('player_1', models.ForeignKey(related_name='league_player_1', to='tc_player.Player')),
                ('player_2', models.ForeignKey(related_name='league_player_2', to='tc_player.Player')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('big_p', models.IntegerField(default=0)),
                ('small_p', models.IntegerField(default=0)),
                ('pair', models.ForeignKey(to='tc_league.Pair')),
                ('player', models.ForeignKey(related_name='league_player', to='tc_player.Player')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
