# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tc_league', '0005_league'),
    ]

    operations = [
        migrations.AddField(
            model_name='league',
            name='judges',
            field=models.ManyToManyField(related_name='league_judge', null=True, verbose_name=b'Judge', to=settings.AUTH_USER_MODEL, blank=True),
            preserve_default=True,
        ),
    ]
