# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_league', '0016_result_achievements'),
    ]

    operations = [
        migrations.AlterField(
            model_name='result',
            name='army',
            field=models.ForeignKey(default=1, to='tc_base.Army'),
            preserve_default=True,
        ),
    ]
