# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_base', '0004_army'),
        ('tc_league', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='pair',
            name='game',
            field=models.ForeignKey(related_name='Game', blank=True, to='tc_base.Games', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='pair',
            name='player_1_army',
            field=models.ForeignKey(related_name='First player army', blank=True, to='tc_base.Army', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='pair',
            name='player_2_army',
            field=models.ForeignKey(related_name='Second player army', blank=True, to='tc_base.Army', null=True),
            preserve_default=True,
        ),
    ]
