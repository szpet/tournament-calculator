from django.contrib import admin

# Register your models here.
from tc_league.models import *

class PairAdmin(admin.ModelAdmin):
    list_display = ("player_1", "player_2")

class ResultAdmin(admin.ModelAdmin):
    list_display = ('player', 'pair', 'big_p', "small_p", 'army')
    list_filter = ("player",)

class LeagueRoundInline(admin.StackedInline):
    model = Round
    extra = 0

class LeagueAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', 'game')
    list_filer = ('name', 'city', 'game')
    inlines = [LeagueRoundInline]

class LeaguePairsInline(admin.StackedInline):
    model = Pair
    extra = 0

class AchievementsAdmin(admin.ModelAdmin):
    list_display = ('name', 'points', 'league')
    list_filter = ("league",)

class RoundAdmin(admin.ModelAdmin):
    pass

admin.site.register(Pair, PairAdmin)
admin.site.register(Result, ResultAdmin)
admin.site.register(League, LeagueAdmin)
admin.site.register(Achievements, AchievementsAdmin)
admin.site.register(Round, RoundAdmin)