from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import timezone

# Create your views here.

from news.models import News
from tc_league.models import Pair
from tc_league.models import Result
from tc_player.models import ITSPin
from tc_player.models import Player
from tc_player.views import UpdatePlayer as PlayerCityForm
from tc_tournament2.models import Tournament
from django.core.urlresolvers import reverse


def getResult(pair, player):
    try:
        return Result.objects.get(player=player, pair=pair)
    except ObjectDoesNotExist:
        return None

def main(request):
    news = News.objects.filter(date__lt=timezone.now()).order_by('-id')[:5]
    data = {'news': news, 'paginate_by': 5}

    tournaments = Tournament.objects.all().order_by('-date')[:5]

    data["tournaments"] = tournaments

    data['city'] = False
    data['itsPin'] = False
    data['emailAgreement'] = False
    data['info'] = []
    if request.user.is_active:
        player = Player.objects.filter(user = request.user).get()
        if player.city.id == 1:
            return HttpResponseRedirect("tc_player/editPlayer/"+str(player.id)+"/")
        if ITSPin.objects.filter(player=player).count() == 0:
            return HttpResponseRedirect(reverse("CreateITSPin"))

    return render(request, 'tc_main/main.html', data)