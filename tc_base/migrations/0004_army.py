# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_base', '0003_city'),
    ]

    operations = [
        migrations.CreateModel(
            name='Army',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=250, null=True)),
                ('visible', models.BooleanField(default=True)),
                ('game', models.ForeignKey(to='tc_base.Games')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
