from django.shortcuts import render
from django.views.generic.edit import CreateView
from django.core.urlresolvers import reverse
from tc_base.models import City

# Create your views here.


class CityView(CreateView):
    model = City
    fields = ['name']

    def get_success_url(self):
        return reverse("updatePlayer", args=(self.request.user.id,))
