from django.db import models

# Create your models here.

class Games(models.Model):
    name = models.CharField(max_length=250, null=True)
    visible = models.BooleanField(default=True)

    def __str__(self):
        return u"{0}".format(self.name)

    def __unicode__(self):
        return u"{0}".format(self.name)

class City(models.Model):
    name = models.CharField(max_length=250, null=True)
    visible = models.BooleanField(default=True)

    def __str__(self):
        return u"{0}".format(self.name)

    def __unicode__(self):
        return u"{0}".format(self.name)


class Army(models.Model):
    name = models.CharField(max_length=250, null=True)
    visible = models.BooleanField(default=True)
    game = models.ForeignKey(Games)

    def __str__(self):
        return u"{0}".format(self.name)

    def __unicode__(self):
        return u"{0}".format(self.name)