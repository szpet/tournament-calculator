from django.conf.urls import url


from tc_base.views import CityView

urlpatterns = [
    url(r'addCity', CityView.as_view(success_url="tc_player/editPlayer/(?P<username>\w+)/"), name="city-new")
    ]