from django.contrib import admin

from models import Games
from models import City
from models import Army

# Register your models here.

class ArmyAdmin(admin.StackedInline):
  model = Army
  extra = 0


class GameAdmin(admin.ModelAdmin):
    inlines = [ArmyAdmin]


class CityAdmin(admin.ModelAdmin):
    pass


class ArmyAdmin(admin.ModelAdmin):
    pass

admin.site.register(Games, GameAdmin)
admin.site.register(City, CityAdmin)