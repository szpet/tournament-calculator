from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from ajax_select import urls as ajax_select_urls
from django.views.i18n import javascript_catalog
admin.autodiscover()

from tc_main import views as MainView

js_info_dict = {
    'packages': ('python_tournamentCalculator',),
}


urlpatterns = [
    url(r'^$', MainView.main),
    url(r'^ajax_select/', include(ajax_select_urls)),
    url(r'^tc2/', include('tc_tournament2.urls')),
    url(r'^tc_league/', include('tc_league.urls')),
    url(r'^tc_player/', include('tc_player.urls')),
    url(r'^tc_base/', include('tc_base.urls')),
    url(r'^news/', include('news.urls')),
    url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/login/'}, name='logout'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^my_admin/jsi18n/$', javascript_catalog, js_info_dict, name='javascript-catalog'),
]
