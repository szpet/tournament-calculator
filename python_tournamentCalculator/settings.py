"""
Django settings for python_tournamentCalculator project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import django.conf.global_settings as DEFAULT_SETTINGS
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'v)x-i6$8+4=j-k+y89)n5$8dvfrzfb70_1as_2r-+3hxy&@bd3'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['127.0.0.1', '195.181.209.144', 'tc.infinity.wroclaw.pl']


# Application definition

INSTALLED_APPS = (
    'ajax_select',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'news',
    'tc_tournament2',
    'tc_player',
    'tc_base',
    'tc_league',
    'tc_main',

)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

DEFAULT_INDEX_TABLESPACE = "db"

ROOT_URLCONF = 'python_tournamentCalculator.urls'

WSGI_APPLICATION = 'python_tournamentCalculator.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',     # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),                                      # Or path to database file if using sqlite3.
    }

    # 'default': {
    #     'ENGINE': 'django.db.backends.mysql',     # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
    #     'NAME': 'asar_tc',                                      # Or path to database file if using sqlite3.
    #     'USER': 'asar_tc',
    #     'PASSWORD': '******',
    #     'HOST': 'mysql5',                     # Empty for localhost through domain sockets or           '127.0.0.1' for localhost through TCP.
    #     'PORT': '3306',                                             # Set to empty string for default.
    # }

}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
TIME_ZONE = 'Europe/Warsaw'

LANGUAGE_CODE = 'en'

DEFAULT_CHARSET = 'utf-8'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
#STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = (os.path.join(BASE_DIR, "static"), )

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR,'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request'
            ],
        },
    },
]

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
)

SITE_ID = 1

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'poczta-692030.vipserv.org'
EMAIL_PORT = '465'
EMAIL_HOST_USER = 'turniej@infinity.wroclaw.pl'
EMAIL_HOST_PASSWORD = 'xxxxx'
EMAIL_USE_TLS = True

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
