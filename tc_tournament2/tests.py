from django.test import TestCase
from django.contrib.auth.models import User
from django.test import Client

from datetime import date

from models import Tournament
from models import Result
from models import Pair
from models import Round
from tc_player.models import Player
from tc_base.models import Games
from tc_base.models import City
import views

# Create your tests here.

class TestCaseForTournament(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestCaseForTournament, cls).setUpClass()

    def setUp(self):
        self.user1 = User.objects.create_user(username='jacob1', email='jacob1@test.pl', password='top_secret')
        self.user2 = User.objects.create_user(username='jacob2', email='jacob2@test.pl', password='top_secret')
        self.player_1 = Player.objects.create(user=self.user1)
        self.player_2 = Player.objects.create(user=self.user2)
        self.game = Games.objects.create(name="Test Game")
        self.city = City.objects.create(name="Test Game")


        self.tournament = Tournament.objects.create(
            id=1,
            date=date(2017,9,27),
            name="Test Tournament",
            status="new",
            owner=self.player_1,
            judges=[self.player_1],
            game=self.game,
            city=self.city,
            statureUrl="http://onet.pl",
            max_number_of_players=16
        )

        self.results = []
        for x in range(1,10,2):
            r = Round.objects.create(number=x, tournaments=self.tournament)
            pair = Pair.objects.create(player_1=self.player_1,
                                       player_2=self.player_2,
                                       round=r)
            self.results.append(Result.objects.create(small_p=x,
                                                      big_p=(x*10),
                                                      tournament_p=(x*100),
                                                      pair=pair,
                                                      player=self.player_1,
                                                      round=r,
                                                      tournaments=self.tournament
                                                      ))

    @classmethod
    def tearDownClass(cls):

        super(TestCaseForTournament, cls).tearDownClass()

    def test_getSumOfResults(self):
        sum = views.get_sum_of_points(self.tournament, self.player_1)
        self.assertEquals({'tournament_p': 2500, 'big_p': 250, 'small_p': 25},sum)
