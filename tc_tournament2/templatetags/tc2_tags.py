from django import template

from django.template.context_processors import csrf
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse

from django.template.loader import render_to_string
from django.utils.safestring import mark_safe

from tc_player.models import ITSPin
from tc_player.models import Player
from tc_tournament2.models import Application
from tc_tournament2.form import AddPlayerForm

register = template.Library()


def getPlayer(user):
    return Player.objects.get(user=user)

def is_owner(tournament, user):
    return tournament.owner.user.id == user.id

def get_start_button(tournament, user):
    if not is_owner(tournament, user):
        return ""
    if tournament.status == "new":
        return u'<a href="{0}" class="button">Start</a>'.format(reverse("tc2_tournament_start", args=(tournament.id,)))
    elif tournament.status == "ongoing":
        return u'<a href="{0}" class="button">Stop</a>'.format(reverse("tc2_tournament_stop", args=(tournament.id,)))
    else:
        return ""

@register.simple_tag()
def get_subscribe_me_button(tournament, user):
    if user.is_anonymous():
        return ""
    try:
        Application.objects.filter(tournament=tournament, player=getPlayer(user)).get()
        if tournament.status == "new":
            out = u'<a href="{0}?tid={1}" class="button">Sign me out</a>'.format(reverse("tc2_unsubscribe_me"), tournament.id)
            out += u'<a href="{0}" class="button">Assign army</a>'.format(reverse("tc2_assigneArmy", args=(tournament.id,)))
            out += u'<a href="{0}" class="button">Add lists</a>'.format(reverse("tc2_add_army_lists", args=(tournament.id,)))
            return mark_safe(out)
    except ObjectDoesNotExist:
        if tournament.status == "new":
            return mark_safe(u'<a href="{0}?tid={1}" class="button">Sign me in</a>'.format(reverse("tc2_subscribe_me"), tournament.id))
    return ""


def create_add_round_form(context, tournament, user):
    if not is_owner(tournament, user):
        return ""
    if tournament.status == "ongoing":
        addRound = render_to_string('tc_tournament2/addRoundForm.html', request=context['request'], context={'tid': tournament.id})
        return mark_safe(addRound)
    return ""

# def add_player_form(context, tournament, user):
#     if not is_owner(tournament, user):
#         return ""
#     if tournament.status == "new":
#         ap_form = render_to_string('tc_tournament2/add_player_form.html', request=context['request'], context={'tid': tournament.id, 'addPlayerForm': AddPlayerForm })
#         return mark_safe(ap_form)
#     return ""


def delete_player_form(context, tournament, user):
    if not is_owner(tournament, user):
        return ""
    if tournament.status == "new":
        ap_form = render_to_string('tc_tournament2/delete_player_form.html', request=context['request'], context={'tid': tournament.id, 'players': Application.objects.filter(tournament=tournament)})
        return mark_safe(ap_form)
    return ""


def fee_form(context, tournament, user):
    if not is_owner(tournament, user):
        return ""
    if tournament.status == "new":
        ap_form = render_to_string('tc_tournament2/fee_form.html', request=context['request'], context={'tid': tournament.id, 'players': Application.objects.filter(tournament=tournament)})
        return mark_safe(ap_form)
    return ""


def get_edit_button(tournament, user):
    if tournament.status == "new" and tournament.owner.user == user:
        return u'<a href="{0}" class="button">Edit</a>'.format(reverse("tc2_editTournament", args=(tournament.id,)))
    return ""

def get_add_player_button(tournament, user):
    if tournament.status == "new" and tournament.owner.user == user:
        return u'<a href="{0}?tid={1}" class="button">Add Player</a>'.format(reverse("tc2_addPlayer"), tournament.id)
    return ""

@register.simple_tag(takes_context=True)
def createActionsButtons(context, tournament, user):
    if user.is_anonymous():
        return ""
    out = get_subscribe_me_button(tournament, user)
    out += get_start_button(tournament, user)
    out += get_edit_button(tournament, user)
    out += get_add_player_button(tournament, user)
    out += u'<table class="paleblue">'
    out += create_add_round_form(context, tournament, user)
    #out += add_player_form(context, tournament, user)
    out += delete_player_form(context, tournament, user)
    out += fee_form(context, tournament, user)
    out += u'</table>'
    return mark_safe(out)


@register.simple_tag()
def get_its_pin(player):
    try:
        pin = ITSPin.objects.get(player=player)
        return pin.its_pin
    except ObjectDoesNotExist:
        return "--"