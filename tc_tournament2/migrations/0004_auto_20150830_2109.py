# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_tournament2', '0003_auto_20150830_1921'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='army',
            field=models.ForeignKey(verbose_name=b'Army', blank=True, to='tc_base.Army', null=True),
        ),
    ]
