# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_tournament2', '0005_tournament_max_number_of_layers'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tournament',
            old_name='max_number_of_layers',
            new_name='max_number_of_players',
        ),
    ]
