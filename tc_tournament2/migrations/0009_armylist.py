# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-26 06:54
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tc_tournament2', '0008_auto_20170917_1955'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArmyList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField(max_length=250, verbose_name=b'Url')),
                ('application', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tc_tournament2.Application', verbose_name=b'Application')),
            ],
        ),
    ]
