# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('tc_player', '0006_itspin'),
        ('tc_base', '0004_army'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Application',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('application_date', models.DateTimeField(verbose_name=b'Application Date')),
                ('fee', models.BooleanField(verbose_name=b'Fee')),
                ('army', models.ForeignKey(verbose_name=b'Army', to='tc_base.Army')),
                ('player', models.ForeignKey(verbose_name=b'Player', to='tc_player.Player')),
            ],
        ),
        migrations.CreateModel(
            name='Pair',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('player_1', models.ForeignKey(related_name='tc2player_1', to='tc_player.Player')),
                ('player_2', models.ForeignKey(related_name='tc2player_2', to='tc_player.Player')),
            ],
        ),
        migrations.CreateModel(
            name='Result',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('big_p', models.IntegerField(default=0)),
                ('small_p', models.IntegerField(default=0)),
                ('pair', models.ForeignKey(to='tc_tournament2.Pair')),
                ('player', models.ForeignKey(related_name='tc2player', to='tc_player.Player')),
            ],
        ),
        migrations.CreateModel(
            name='Round',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Tournament',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField()),
                ('name', models.CharField(max_length=250)),
                ('status', models.CharField(default=b'new', max_length=50, verbose_name=b'Status', choices=[(b'new', b'before starting'), (b'ongoing', b'ongoing'), (b'completed', b'completed')])),
                ('statureUrl', models.URLField(default=b'', verbose_name=b'Url to statue')),
                ('city', models.ForeignKey(related_name='tc2city', to='tc_base.City')),
                ('game', models.ForeignKey(related_name='tc2game', blank=True, to='tc_base.Games', null=True)),
                ('judges', models.ManyToManyField(related_name='tc2judge', null=True, verbose_name=b'Judge', to=settings.AUTH_USER_MODEL, blank=True)),
                ('owner', models.ForeignKey(related_name='tc2owner', default=1, verbose_name=b'Owner', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='round',
            name='tournaments',
            field=models.ForeignKey(to='tc_tournament2.Tournament'),
        ),
        migrations.AddField(
            model_name='result',
            name='round',
            field=models.ForeignKey(blank=True, to='tc_tournament2.Round', null=True),
        ),
        migrations.AddField(
            model_name='result',
            name='tournaments',
            field=models.ForeignKey(blank=True, to='tc_tournament2.Tournament', null=True),
        ),
        migrations.AddField(
            model_name='pair',
            name='round',
            field=models.ForeignKey(to='tc_tournament2.Round'),
        ),
        migrations.AddField(
            model_name='application',
            name='tournament',
            field=models.ForeignKey(verbose_name=b'Tournament', to='tc_tournament2.Tournament'),
        ),
        migrations.AlterUniqueTogether(
            name='result',
            unique_together=set([('player', 'round')]),
        ),
    ]
