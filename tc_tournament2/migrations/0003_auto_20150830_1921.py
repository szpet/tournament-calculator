# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_tournament2', '0002_auto_20150830_1517'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='application',
            unique_together=set([('tournament', 'player')]),
        ),
    ]
