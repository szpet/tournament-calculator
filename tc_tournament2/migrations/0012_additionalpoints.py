# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-03 19:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tc_player', '0007_auto_20170917_1955'),
        ('tc_tournament2', '0011_tournament_additionalpointscategory'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdditionalPoints',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tournament_p', models.IntegerField(default=0)),
                ('player', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tc_player.Player', verbose_name=b'Player')),
                ('tournament', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tc_tournament2.Tournament', verbose_name=b'Tournament')),
            ],
        ),
    ]
