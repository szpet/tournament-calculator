# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_tournament2', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tournament',
            name='judges',
            field=models.ManyToManyField(related_name='tc2judge', null=True, verbose_name=b'Judge', to='tc_player.Player', blank=True),
        ),
        migrations.AlterField(
            model_name='tournament',
            name='owner',
            field=models.ForeignKey(related_name='tc2owner', default=1, verbose_name=b'Owner', to='tc_player.Player'),
        ),
    ]
