# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_tournament2', '0004_auto_20150830_2109'),
    ]

    operations = [
        migrations.AddField(
            model_name='tournament',
            name='max_number_of_layers',
            field=models.IntegerField(default=b'1000', verbose_name=b'Max number of players'),
        ),
    ]
