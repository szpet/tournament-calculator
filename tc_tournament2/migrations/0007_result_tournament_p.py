# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tc_tournament2', '0006_auto_20150905_0708'),
    ]

    operations = [
        migrations.AddField(
            model_name='result',
            name='tournament_p',
            field=models.IntegerField(default=0),
        ),
    ]
