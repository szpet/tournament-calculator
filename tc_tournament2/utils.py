import os
from django.template.loader import select_template


def get_template_name(object, app_dir, template_file):
        game_dir = str(object.game).strip().replace(" ", "_")
        default_template = os.path.join(app_dir, template_file)
        game_template = os.path.join(app_dir, game_dir, template_file)
        search_result = select_template([game_template, default_template])
        template_name = search_result.template.name
        return template_name
