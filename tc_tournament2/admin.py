from django.contrib import admin

from tc_tournament2.models import Tournament
from tc_tournament2.models import Application
from tc_tournament2.models import Round
from tc_tournament2.models import Pair
from tc_tournament2.models import Result
from tc_tournament2.models import AdditionalPointsCategory
from tc_tournament2.models import AdditionalPoints
# Register your models here.


class RoundAdmin(admin.ModelAdmin):
    pass


class ResultInline(admin.StackedInline):
    model = Result
    extra = 0


class RoundInline(admin.StackedInline):
    model = Round
    extra = 0

class ArmyConnect(admin.StackedInline):
    model = Application
    extra = 0


class TourmentAdmin(admin.ModelAdmin):
    list_display = ("name", "date", "status", "owner", "game", "city")
    list_filter = ("status","date", "city")
    inlines = [ArmyConnect, RoundInline, ResultInline]


class PairAdmin(admin.ModelAdmin):
    pass


class ResultAdmin(admin.ModelAdmin):
    list_display = ("tournaments", "round", "player", "pair", "big_p", "small_p")
    list_filter = ("tournaments", "round", "player")


class FeeAdmin(admin.ModelAdmin):
    list_display = ("tournament", "player", "fee")
    list_filter = ("tournament", "player", "fee")


class AdditionalPointsCategoryAdmin(admin.ModelAdmin):
    pass

class AdditionalPointsAdmin(admin.ModelAdmin):
    list_display = ("tournament", "player", "additionalPointsCategory", "tournament_p")

admin.site.register(Round, RoundAdmin)
admin.site.register(Pair, PairAdmin)
admin.site.register(Tournament, TourmentAdmin)
admin.site.register(Result, ResultAdmin)
admin.site.register(AdditionalPointsCategory, AdditionalPointsCategoryAdmin)
admin.site.register(AdditionalPoints, AdditionalPointsAdmin)