from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.db.models import Count, Min, Sum, Avg
from django.db.models.functions import Coalesce
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView

import datetime
import math
import pprint

from tc_base.models import Army
from tc_base.models import City
from tc_player.models import Player
from tc_tournament2.models import Application
from tc_tournament2.models import Result
from tc_tournament2.models import Round
from tc_tournament2.models import Tournament
from tc_tournament2.models import Pair
from tc_tournament2.models import ArmyList
from tc_tournament2.models import AdditionalPointsCategory
from tc_tournament2.models import AdditionalPoints
from tc_tournament2.form import NewTournamentViewForm
from tc_tournament2.form import AddPlayerForm
from tc_tournament2.utils import get_template_name

# Create your views here.


class TournamentListView(ListView):
    queryset = Tournament.objects.order_by('-id')
    model = Tournament


class NewTournamentView(CreateView):
    model = Tournament
    form_class = NewTournamentViewForm

    def form_valid(self, form):
        f = form.save()
        f.save()
        self.id = f.id
        judge = Player.objects.get(user=self.request.user)
        f.judges.add(judge)
        f.owner = judge
        f.stats = "new"
        f.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("tc2_tournament_details", args=(self.id,))


class TournamentUpdate(UpdateView):
    model = Tournament
    template_name_suffix = '_update_form'
    form_class = NewTournamentViewForm

    def get_success_url(self):
        return reverse("tc2_tournament_details", args=(self.object.id,))

class TournamentDetails(DetailView):
    model = Tournament

    def get_object(self):
        object = super(TournamentDetails, self).get_object()
        applications = Application.objects.filter(tournament_id=object.id).order_by("application_date")

        self.template_name = get_template_name(object, 'tc_tournament2', 'tournament_detail.html')
        sorted_applications = []
        for a in applications:
            a.points = get_sum_of_points(object, a.player)
            a.army_lists = a.armylist_set.all()

        sorted_applications = sorted(applications, cmp=lambda p1,p2: cmp_points(p1,p2),  reverse=True)
        object.application = sorted_applications
        object.rounds = Round.objects.filter(tournaments_id=object.id).order_by("-id")

        for round in object.rounds:
            pairs = Pair.objects.filter(round=round).all().prefetch_related('player_1', 'player_2')
            round.pairs = []
            for pair in pairs:
                results = Result.objects.filter(pair=pair).order_by("-round").all().prefetch_related('player')
                for result in results:
                    if result.player == pair.player_1:
                        pair.player_1_res = result
                        pair.canPlayer1AddResult = self.request.user == pair.player_1.user
                    elif result.player == pair.player_2:
                        pair.player_2_res = result
                        pair.canPlayer2AddResult = self.request.user == pair.player_2.user
                round.pairs.append(pair)
        object.canAddResultVal = can_user_add_result(object, self.request.user)

        return object


def get_sum_of_points(tournament, player):
    results = Result.objects.filter(tournaments=tournament, player=player)
    additionalPoints = AdditionalPoints.objects.filter(tournament=tournament, player=player).aggregate(sum=Coalesce(Sum('tournament_p'),0))
    big_p = 0
    small_p = 0
    tournament_p = 0
    for result in results:
        big_p += result.big_p
        small_p += result.small_p
        tournament_p += result.tournament_p
    sum = tournament_p
    additionalPoints_ = 0
    try:
        sum += additionalPoints['sum']
        additionalPoints_ = additionalPoints['sum']
    except KeyError as e:
        pass
    except AttributeError as e:
        pass
    return {'big_p': big_p, 'small_p': small_p, 'tournament_p': tournament_p, 'additionalPoints': additionalPoints_, 'sum': sum}


def cmp_points(p1, p2):
    if p1.points['sum'] != p2.points['sum']:
        return int(p1.points['sum'] - p2.points['sum'])
    elif p1.points['tournament_p'] != p2.points['tournament_p']:
        return int(p1.points['tournament_p'] - p2.points['tournament_p'])
    elif p1.points['big_p'] != p2.points['big_p']:
        return int(p1.points['big_p'] - p2.points['big_p'])
    else:
        return int(p1.points['small_p'] - p2.points['small_p'])


def __isJudge(self, user):
    q = self.judges.all().values_list('id',  flat=True)
    return user.id in q


def can_user_add_result(tournament, user):
    if user.is_anonymous():
        return False
    player = Player.objects.get(user=user)
    return player == tournament.owner or player in tournament.judges.all()


class ApplicationView(CreateView):
    model = Application
    fields = ["army"]

    def get_form(self, form_class=None):
        form = super(ApplicationView, self).get_form()
        tid = self.request.GET['tid']
        tournament = Tournament.objects.get(id=tid)
        form.fields['army'].queryset = Army.objects.filter(game=tournament.game, visible=True)
        form.tid = tid
        return form

    def form_valid(self, form):
        f = form.save(commit=False)
        tid = self.request.GET['tid']
        f.tournament_id = tid
        f.player = Player.objects.get(user=self.request.user)
        f.application_date = datetime.datetime.now()
        f.fee = False
        f.save()
        return HttpResponseRedirect(reverse("tc2_tournament_details", args=(tid,)))


def unsubscribe_me(request):
    tid = request.GET['tid']
    player = Player.objects.get(user=request.user)
    Application.objects.filter(tournament_id=tid, player=player).delete()
    return HttpResponseRedirect(reverse("tc2_tournament_details", args=(tid,)))


def add_player(request):
    if request.method == 'POST':
        tid = request.GET['tid']
        player = Player.objects.get(id=request.POST['player'])
        Application(tournament_id=tid, player=player, application_date=datetime.datetime.now(), fee=False).save()
        return HttpResponseRedirect(reverse("tc2_tournament_details", args=(tid,)))
    else:
        f = AddPlayerForm()
        return render(request, 'tc_tournament2/add_player_form.html', {'form': f, 'tid': request.GET['tid'] })




def delete_player(request):
    tid = request.GET['tid']
    Application.objects.get(id=request.POST['ApplicationId']).delete()
    return HttpResponseRedirect(reverse("tc2_tournament_details", args=(tid,)))


def assigneArmy(request, id, error=False):
    if request.user.is_anonymous():
        return HttpResponseRedirect("tc/tournamentDetail/"+str(id)+"/")

    tournament = Tournament.objects.filter(id=id).get()
    player = Player.objects.filter(user=request.user).get()
    armies = Army.objects.filter(game=tournament.game).all()
    application = Application.objects.filter(tournament=tournament, player=player).get()
    current_army = application.army
    return render(request, "tc_tournament2/assigneArmy.html", {
        'armies':armies,
        'current_army': current_army,
        'tournamentId':id,
        'playerId': player.id,
        'error': error
    })


def changeArmy(request):
    if request.method == 'POST':
        try:
            t = Application.objects.get(player_id=request.POST["playerId"], tournament_id=request.POST["tourmentId"])
            t.army = Army.objects.get(id=request.POST['selectedArmy'])
            t.save()
        except ObjectDoesNotExist:
            t = Application(player_id=request.POST["playerId"], tournament_id=request.POST["tourmentId"], army_id=request.POST['selectedArmy'])
            t.save()
        return HttpResponseRedirect(reverse("tc2_tournament_details", args=(request.POST["tourmentId"],)))
    return HttpResponseRedirect(reverse("tc2_list"))


def add_army_lists(request, pk):
    player = Player.objects.filter(user=request.user).get()
    tournament = Tournament.objects.get(pk=pk)
    try:
        application = Application.objects.filter(tournament=tournament, player=player).get()
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse("tc2_tournament_details", args=(pk,)))

    if request.method == 'POST':
        application.armylist_set.all().delete()
        for list_url in request.POST.getlist('army_lists'):
            army_list = ArmyList(application=application, url=list_url)
            army_list.save()
        return HttpResponseRedirect(reverse("tc2_tournament_details", args=(pk,)))

    template = get_template_name(tournament, 'tc_tournament2', 'add_army_lists.html')
    context = {'army_lists': application.armylist_set.all()}
    return render(request, template, context)


def fee(request):
    if request.method == 'POST':
        try:
            t = Application.objects.get(id=request.POST['ApplicationId'])
            if t.fee is True:
                t.fee = False
            else:
                t.fee = True
            t.save()
        except ObjectDoesNotExist:
            return HttpResponseRedirect(reverse("tc2_tournament_details", args=(request.GET["tid"],)))
        return HttpResponseRedirect(reverse("tc2_tournament_details", args=(request.GET["tid"],)))
    return HttpResponseRedirect(reverse("tc2_list"))


def add_bay_player(tournament):
    Application(tournament=tournament, player_id=2, application_date=datetime.datetime.now(), fee=False).save()


def start_tournament(request, pk):
    try:
        t = Tournament.objects.get(pk=pk)
        t.status = "ongoing"
        t.save()
        if Application.objects.filter(tournament=t).count() % 2 is 1:
            add_bay_player(t)
        return HttpResponseRedirect(reverse("tc2_tournament_details", args=(pk,)))
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse("tc2_list"))


def calculate_bay(tournament, pair, player):
    player_points = get_sum_of_points(tournament, player)
    bay_results = Result.objects.get(pair=pair,player=player)

    num_of_rounds = Round.objects.filter(tournaments=tournament).count()

    bay_results.big_p = (player_points["big_p"] * num_of_rounds) / (num_of_rounds - 1)
    bay_results.small_p = (player_points["small_p"] * num_of_rounds) / (num_of_rounds - 1)
    bay_results.tournament_p = 2
    bay_results.save()


def calculate_bays(tournament):
    pairs_bay = Pair.objects.filter(Q(round__tournaments=tournament), Q(player_1__id=2) | Q(player_2__id=2))
    for pair in pairs_bay:
        player = None
        if pair.player_1.id == 2:
            player = pair.player_2
        elif pair.player_2.id == 2:
            player = pair.player_1
        calculate_bay(tournament, pair, player)


def completed_tournament(request, pk):
    try:
        t = Tournament.objects.get(pk=pk)
        rounds = Round.objects.filter(tournaments=t.id).order_by("-number")
        calculate_victoryPoints(request, rounds[0].pk)
        calculate_bays(t)
        t.status = "completed"
        t.save()
        return HttpResponseRedirect(reverse("tc2_tournament_details", args=(pk,)))
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse("tc2_list"))


class PlayerResult:
    def __init__(self, _player):
        self.player = _player
        self.big_p = 0
        self.small_p = 0
        self.tournament_p = 0

    def __str__(self):
        return u'{0}: {3} {1}[{2}]'.format(self.player, self.big_p, self.small_p, self.tournament_p)

    def __unicode__(self):
        return u'{0}: {3} {1}[{2}]'.format(self.player, self.big_p, self.small_p, self.tournament_p)


def can_players_play_each_other(player_1_result, player_2_result, tournament_id):
    pairs = Pair.objects.filter(Q(round__tournaments=tournament_id), Q(player_1=player_1_result.player) | Q(player_2=player_1_result.player))

    result = True
    for pair in pairs:
        if pair.player_1 == player_2_result.player or pair.player_2 == player_2_result.player:
            result = False
    return result


def build_pairs_history(players_results, tournament_id):
    players_result_list = []
    for p in players_results:
        players_result_list.append(p)
    pairs = []
    while len(players_result_list) != 0:
        index = 0
        player_1_result = players_result_list.pop(0)
        player_2_result = players_result_list[index]
        if len(players_result_list) == 1:
            pair = Pair(player_1=player_1_result.player, player_2=player_2_result.player)
        else:
            while not can_players_play_each_other(player_1_result, player_2_result, tournament_id):
                index = index + 1
                player_2_result = players_result_list[index]
            pair = Pair(player_1=player_1_result.player, player_2=player_2_result.player)
        players_result_list.pop(index)
        pairs.append(pair)
    return pairs


def update_city_count(cityList, player_1, player_2):
    cityList[player_1.city.id] = cityList[player_1.city.id] - 1
    cityList[player_2.city.id] = cityList[player_2.city.id] - 1

def it_has_been_more_than_one_city(cityList):
    activeCityCounter = 0
    for k,v in cityList.iteritems():
        if v != 0:
            activeCityCounter = activeCityCounter + 1
    return activeCityCounter > 1


def are_players_form_the_same_city(player_1, player_2):
    return player_1.city.id == player_2.city.id


def build_pairs_city(players_result):
    players_result_list = []
    cityList = {}
    citys = City.objects.all()
    for city in citys:
        cityList[city.id] = 0
    for p in players_result:
        players_result_list.append(p)
        cityList[p.player.city.id] = cityList[p.player.city.id] + 1
    pairs = []
    while len(players_result_list) != 0:
        index = 0
        player_1_result = players_result_list.pop(0)
        player_2_result = players_result_list[index]
        if len(players_result_list) == 1:
            pair = Pair(player_1=player_1_result.player, player_2=player_2_result.player)
            update_city_count(cityList, player_1_result.player, player_2_result.player)
        else:
            while are_players_form_the_same_city(player_1_result.player, player_2_result.player) and it_has_been_more_than_one_city(cityList):
                index = index + 1
                player_2_result = players_result_list[index]
            pair = Pair(player_1=player_1_result.player, player_2=player_2_result.player)
            update_city_count(cityList, player_1_result.player, player_2_result.player)
        players_result_list.pop(index)
        pairs.append(pair)
    return pairs


def make_pairs(players):
    pairs = []
    players_list = []
    for p in players:
        players_list.append(p)
    index = 0
    while len(players_list):
        player_1 = players_list.pop(0).player
        player_2 = players_list.pop(0).player
        pair = Pair(player_1=player_1, player_2=player_2)
        pairs.append(pair)
        index = index + 1
    return pairs


def cmp_points1(p1, p2):
    if p1.big_p == p2.big_p:
        return int(p1.small_p - p2.small_p)
    return int(p1.big_p - p2.big_p)


def get_players_results(tournament):
    players = []
    for application in Application.objects.filter(tournament=tournament):
        player_result = PlayerResult(application.player)
        for result in Result.objects.filter(player=player_result.player,tournaments=tournament):
            player_result.big_p += result.big_p
            player_result.small_p += result.small_p
            player_result.tournament_p += result.tournament_p
        players.append(player_result)
    return players


def add_round(request, pk):
    if request.method == 'POST':
        tournament = Tournament.objects.get(pk=pk)
        players_results = get_players_results(tournament)
        roundsCount = Round.objects.filter(tournaments=pk).count()
        if roundsCount > 0:
            players_results = sorted(players_results, cmp=lambda p1,p2: cmp_points1(p1,p2),  reverse=True)
        methodId = int(request.POST.get('methodId'))
        if methodId == 2:
            pairs = build_pairs_history(players_results, tournament.id)
        elif methodId == 3:
            pairs = build_pairs_city(players_results)
        else:
            pairs = make_pairs(players_results)
        return render(request, "tc_tournament2/pairForm.html", {'pairs': pairs, 'players': players_results, 'tournamentId': tournament.id})

def savePairs(request, pk):
    t = Tournament.objects.get(pk=pk)
    tournament_id = t.id
    rounds = Round.objects.filter(tournaments=tournament_id).order_by("-number")
    if(len(rounds) > 0):
        calculate_victoryPoints(request, rounds[0].pk)
    tournament = Tournament.objects.get(id=tournament_id)
    my_round = Round(tournaments=tournament, number=(len(rounds) + 1))
    my_round.save()

    pairs = {}

    for k,v in request.POST.iteritems():
        if k.startswith("pair"):
            pairKey = k.split(".")[1]
            if not pairKey in pairs:
                pairs[pairKey] = []
            pairs[pairKey].append(v)

    for v in pairs.values():
        pair = Pair()
        pair.player_1 = Player.objects.get(id=v[0])
        pair.player_2 = Player.objects.get(id=v[1])
        pair.round = my_round
        pair.save()
        Result(big_p=0, small_p=0, pair=pair, player=pair.player_1, round=my_round, tournaments=t).save()
        Result(big_p=0, small_p=0, pair=pair, player=pair.player_2, round=my_round, tournaments=t).save()

    return HttpResponseRedirect(reverse("tc2_tournament_details", args=(pk,)))

def submit_result_by_admin(request, pk):
    if request.method == 'POST':
        if len(request.POST.get('big_p')) == 0 or len(request.POST.get('small_p')) == 0:
            msgs = []
            msgs.append("Please specify results")
            request.session["error_msg"] = msgs
            return HttpResponseRedirect(reverse("tc2_tournament_details", args=(pk,)))
        pair = Pair.objects.get(id=request.POST.get('pairId'))
        player = Player.objects.get(id=request.POST.get('playerId'))
        res = Result()
        try:
            res = Result.objects.filter(pair=pair, player=player).get()
        except res.DoesNotExist:
            res.pair = pair
            res.player = player

        res.big_p = request.POST.get('big_p')
        res.small_p = request.POST.get('small_p')
        if 'tournament_p' in request.POST:
            res.tournament_p = request.POST.get('tournament_p')
        res.round = pair.round
        res.tournaments = pair.round.tournaments
        res.save()
    return HttpResponseRedirect(reverse("tc2_tournament_details", args=(pk,)))


def calculate_victoryPoints(request, pk):
    for pair in Pair.objects.filter(round=pk):
        p1_result = Result.objects.get(round=pk, player=pair.player_1)
        p2_result = Result.objects.get(round=pk, player=pair.player_2)
        calculatePointsInfinity(p1_result, p2_result)


def calculatePointsInfinity(p1_result, p2_result):
    if p1_result.big_p - p2_result.big_p == 0:
        p1_result.tournament_p = 1
        p1_result.save()
        p2_result.tournament_p = 1
        p2_result.save()
    else:
        if p1_result.big_p > p2_result.big_p:
            calculatePointsInfinityVictory(p1_result, p2_result)
        else:
            calculatePointsInfinityVictory(p2_result, p1_result)


def calculatePointsInfinityVictory(winner_result, lost_result):
    if winner_result.big_p - lost_result.big_p <= 4:
        winner_result.tournament_p = 2
        winner_result.save()
        lost_result.tournament_p = 0
        lost_result.save()
    if winner_result.big_p - lost_result.big_p > 4:
        winner_result.tournament_p = 3
        winner_result.save()
        lost_result.tournament_p = 0
        lost_result.save()


class NewAdditionalPointsCategory(CreateView):
    model = AdditionalPointsCategory
    fields = ['name']

    def get_success_url(self):
        return reverse("tc2_AdditionalPointsCategoryList",)


class AdditionalPointsCategoryListView(ListView):
    queryset = AdditionalPointsCategory.objects.order_by('id')
    model = AdditionalPointsCategory


def AddAdditionalPointsToPlayer(request):
    if request.method == 'POST':
        tid = request.POST.get("id_tournament")
        pid = request.POST.get("id_player")
        acp_val = request.POST.getlist('acp_val')
        acp_id = request.POST.getlist('acp_id')
        for x in range(0, len(acp_val)):
            d = {"val":acp_val[x], "id":acp_id[x]}
            ap, res = AdditionalPoints.objects.get_or_create(tournament_id=tid, player_id=pid, additionalPointsCategory_id=d['id'])
            ap.tournament_p = d['val']
            ap.save()

        additionalPointsCategory, player, tournament = getAddotionalPoints(pid=pid, tid=tid)
        return render(request, "tc_tournament2/AddAdditionalPointsToPlayer.html",
                  {'additionalPointsCategory': additionalPointsCategory, 'player': player, 'tournament': tournament})
    else:
        additionalPointsCategory, player, tournament = getAddotionalPoints(pid=request.GET['p'], tid=request.GET['t'])

        return render(request, "tc_tournament2/AddAdditionalPointsToPlayer.html", {'additionalPointsCategory': additionalPointsCategory, 'player': player, 'tournament': tournament})


def getAddotionalPoints(pid, tid):

    tournamentAdditionalPointsCategoryIds = Tournament.objects.values('additionalPointsCategory').filter(id=tid)
    additionalPointsCategory = AdditionalPointsCategory.objects.filter(id__in=tournamentAdditionalPointsCategoryIds)
    player = Player.objects.get(id=pid)
    tournament = Tournament.objects.get(id=tid)
    for apc in additionalPointsCategory:
        try:
            ap = AdditionalPoints.objects.filter(tournament=tournament, player=player, additionalPointsCategory=apc)[
                0].tournament_p
            apc.value = ap
        except IndexError:
            apc.value = 0;
    return additionalPointsCategory, player, tournament