from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views



urlpatterns = [
        url(r'tc2_list/', views.TournamentListView.as_view(), name="tc2_list"),
        url(r'tc2_addNewTournament/', views.NewTournamentView.as_view(), name="tc2_addNewTournament"),
        url(r'tc2_editTournament/(?P<pk>\d+)/', views.TournamentUpdate.as_view(), name="tc2_editTournament"),
        url(r'tc2_TournamentDetails/(?P<pk>\d+)/$', views.TournamentDetails.as_view(), name="tc2_tournament_details"),
        url(r'tc2_tournament_start/(?P<pk>\d+)/$', views.start_tournament, name="tc2_tournament_start"),
        url(r'tc2_tournament_stop/(?P<pk>\d+)/$', views.completed_tournament, name="tc2_tournament_stop"),
        url(r'tc2_subscribe_me/$', views.ApplicationView.as_view(), name="tc2_subscribe_me"),
        url(r'tc2_unsubscribe_me/$', views.unsubscribe_me, name="tc2_unsubscribe_me"),
        url(r'tc2_addRound/(?P<pk>\d+)/$', views.add_round, name="tc2_addRound"),
        url(r'tc2_addplayer/$', views.add_player, name="tc2_addPlayer"),
        url(r'tc2_deleteplayer/$', views.delete_player, name="tc2_deletePlayer"),
        url(r'tc2_assigneArmy/(\d+)/', views.assigneArmy, name="tc2_assigneArmy"),
        url(r'tc2_changeArmy/', views.changeArmy, name="tc2_changeArmy"),
        url(r'tc2_add_army_lists/(?P<pk>\d+)/$', login_required(views.add_army_lists), name="tc2_add_army_lists"),
        url(r'tc2_fee/', views.fee, name="tc2_fee"),
        url(r'tc2_savePairs/(?P<pk>\d+)/', views.savePairs, name="tc2_savePairs"),
        url(r'tc2_submitResultByAdmin/(?P<pk>\d+)/', views.submit_result_by_admin, name="tc2_submitResultByAdmin"),
        url(r'tc2_calculateVictoryPoints/(?P<pk>\d+)/', views.calculate_victoryPoints, name="tc2_calculateVictoryPoints"),
        url(r'tc2_NewAdditionalPointsCategory/', views.NewAdditionalPointsCategory.as_view(), name="tc2_NewAdditionalPointsCategory"),
        url(r'tc2_AdditionalPointsCategoryListView', views.AdditionalPointsCategoryListView.as_view(), name="tc2_AdditionalPointsCategoryList"),
        url(r'tc2_AddAdditionalPointsToPlayer', views.AddAdditionalPointsToPlayer, name="tc2_AddAdditionalPointsToPlayer")
    ]