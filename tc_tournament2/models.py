from django.contrib.auth.models import User
from django.db import models
from tc_base.models import Army
from tc_base.models import City
from tc_base.models import Games
from tc_player.models import Player

# Create your models here.


class AdditionalPointsCategory(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return u'{}'.format(self.name)

    def __unicode__(self):
        return u'{}'.format(self.name)


class Tournament(models.Model):
    STATE = ( ("new", "before starting"),
              ("ongoing", "ongoing"),
              ("completed", "completed"))
    date = models.DateField()
    name = models.CharField(max_length= 250)
    status = models.CharField(max_length=50, choices=STATE, verbose_name="Status", default="new")
    owner = models.ForeignKey(Player, verbose_name='Owner', default=1, related_name="tc2owner")
    judges = models.ManyToManyField(Player, verbose_name="Judge", blank=True, related_name="tc2judge")
    additionalPointsCategory = models.ManyToManyField(AdditionalPointsCategory, verbose_name="Additional Points Category", blank=True, related_name="tc2apc")
    game = models.ForeignKey(Games, blank=True, null=True, related_name="tc2game")
    city = models.ForeignKey(City, blank=False, null=False, related_name="tc2city")
    statureUrl = models.URLField(blank=False, null=False, default="", verbose_name="Url to statue")
    max_number_of_players = models.IntegerField(default="1000", verbose_name="Max number of players")

    def __str__(self):
        return u'{0}'.format(self.name)

    def __unicode__(self):
        return u'{0}'.format(self.name)


class AdditionalPoints(models.Model):
    tournament = models.ForeignKey(Tournament, verbose_name="Tournament", null=True)
    player = models.ForeignKey(Player, verbose_name="Player", null=True)
    additionalPointsCategory = models.ForeignKey(AdditionalPointsCategory, verbose_name="Category", null=True)
    tournament_p = models.IntegerField(default=0)


class Application(models.Model):
    tournament = models.ForeignKey(Tournament, verbose_name="Tournament")
    player = models.ForeignKey(Player, verbose_name="Player")
    application_date = models.DateTimeField(verbose_name="Application Date")
    army = models.ForeignKey(Army, verbose_name="Army", blank=True, null=True)
    fee = models.BooleanField(verbose_name="Fee")

    class Meta:
        unique_together = ('tournament', 'player',)


    def __str__(self):
        return u'{0} - {1}'.format(self.tournament, self.player)

    def __unicode__(self):
        return u'{0} - {1}'.format(self.tournament, self.player)


class ArmyList(models.Model):
    application = models.ForeignKey(Application, verbose_name="Application")
    url = models.URLField(max_length=250, verbose_name='Url')

    def __str__(self):
        return u'{0}'.format(self.url)

    def __unicode__(self):
        return u'{0}'.format(self.url)


class Round(models.Model):
    number = models.IntegerField()
    tournaments = models.ForeignKey(Tournament)

    def __str__(self):
        return u'{0} - Round {1}'.format(self.tournaments, self.number)

    def __unicode__(self):
        return u'{0} - Round {1}'.format(self.tournaments, self.number)


class Pair(models.Model):
    player_1 = models.ForeignKey(Player, related_name="tc2player_1")
    player_2 = models.ForeignKey(Player, related_name="tc2player_2")
    round = models.ForeignKey(Round)

    def __str__(self):
        return u'{} {} vs. {}'.format(self.round, self.player_1, self.player_2)

    def __unicode__(self):
        return u'{} {} vs. {}'.format(self.round, self.player_1, self.player_2)


class Result(models.Model):
    big_p = models.IntegerField(default=0)
    small_p = models.IntegerField(default=0)
    tournament_p = models.IntegerField(default=0)
    pair = models.ForeignKey(Pair)
    player = models.ForeignKey(Player, related_name="tc2player")
    round = models.ForeignKey(Round, blank=True, null=True)
    tournaments = models.ForeignKey(Tournament, blank=True, null=True)

    class Meta:
        unique_together = ("player", "round")

    def __str__(self):
        return u'{0} {1} {2} {3}({4})'.format(self.tournaments, self.round, self.player, self.big_p, self.small_p)

    def __unicode__(self):
        return u'{0} {1} {2} {3}({4})'.format(self.tournaments, self.round, self.player, self.big_p, self.small_p)



