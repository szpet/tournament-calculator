from django.forms import DateInput
from django.contrib.admin import widgets
from django.forms import ModelChoiceField
from django.forms import ModelForm

from tc_tournament2.models import Tournament
from tc_player.models import Player
from tc_tournament2.models import Application
from ajax_select.fields import AutoCompleteSelectField, AutoCompleteSelectMultipleField


class NewTournamentViewForm(ModelForm):
    class Meta:
        model = Tournament
        fields = ['date', 'name', 'judges', 'additionalPointsCategory', 'game', 'city', 'max_number_of_players', 'statureUrl']
        widgets = {
            'date': DateInput(),
        }

    judges = AutoCompleteSelectMultipleField('PlayerAutocomplete', required=False, help_text="Automatically filled with the person who will create the tournament.")

    def __init__(self, *args, **kwargs):
        super(NewTournamentViewForm, self).__init__(*args, **kwargs)
        self.fields['date'].widget = widgets.AdminDateWidget()


class AddPlayerForm(ModelForm):
    player = AutoCompleteSelectField('PlayerAutocomplete', required=False, help_text="")

    class Meta:
        model = Application
        fields = ['player']
